var switches = document.getElementsByClassName('ConfigItem__Input');

function handleButtonClick() {
	var skipFilledInput = switches[0].checked;
	var skipTripProtection = switches[1].checked;

	chrome.storage.sync.set({ skipFilledInput }, function () {
		console.log('Set the skipFilledInput to ', skipFilledInput);
	});

	chrome.storage.sync.set({ skipTripProtection }, function () {
		console.log('Set the skipTripProtection to ', skipTripProtection);
	});

}

function constructOptions() {

	for (var i = 0; i < switches.length; i++) {
		switches[i].addEventListener("click", handleButtonClick);
	}

	chrome.storage.sync.get("skipFilledInput", (data) => {
		console.log('Read the skipFilledInput ', data);
		switches[0].checked = data.skipFilledInput;
	});

	chrome.storage.sync.get("skipTripProtection", (data) => {
		console.log('Read the skipTripProtection ', data);
		switches[1].checked = data.skipTripProtection;
	});
}

constructOptions();