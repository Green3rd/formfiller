### Overview ###

* This extension is to automatically fill the form on the webpage.
* Version 1.0
![UI](./ui.jpg)


### How to use ###

* Click the extension, and the extension will fill all inputs in the form 1 second after click the extension.
* Note that some form need the user to actually focus at the input fields. In this case, simply click the form webpage after click the extension. The extension will focus the input before filling the data.

### Configurations ###

* If you don't want the field that already have data to be re-fill the data, turn on the first option on the configutaions page.
![Configurations Page](./configs.jpg)